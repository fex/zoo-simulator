<!doctype html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.4/foundation.css" />
</head>

<body>

<h1>Basic PHP Zoo Simulator</h1>

<a class="button secondary" href="?target=destroy">Start Over</a>  <a class="button primary" href="?target=increment">Increment</a> <a class="button" href="?target=interact&amp;action=feed">Feed</a>
<?php

function __autoload($class_name)
{
    $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);

    include $class_name . '.php';
}


error_log('::::::::::::::::: INDEX :::::::::::::::::');

$options = [
	'animals' => [
		'Monkey',
		'Monkey',
		'Monkey',
		'Monkey',
		'Monkey',
		'Elephant',
		'Elephant',
		'Elephant',
		'Elephant',
		'Elephant',
		'Giraffe',
		'Giraffe',
		'Giraffe',
		'Giraffe',
		'Giraffe',
	]
];

$zoo = new Zoo\Simulator('Zoo',$options);

$output = $zoo->output();

$iterations = $output['iterations'];
echo "
		<h3>Time lapsed: {$iterations} hours</h3>
	";

foreach ($output['animals'] as $animal) {
	$animalHealth = $animal->getHealth();

	$name = $animal->name;
	$minHealth = $animalHealth['minHealth'];
	$maxHealth = $animalHealth['maxHealth'];
	$health = floor($animalHealth['health']*100)/100;
	$status = $animalHealth['status'];
	$alert = $minHealth/$health;
	if($status == 0) {
		$style = "secondary";
	} elseif($status == 1) {
		$style = "alert";
	} elseif($status == 2) {
		$style = "success";
	}
	if($status == 2 && $alert >= 0.8 ) {
		$style = "warning";
	}
	$percentage = floor($health/$maxHealth*100);

	echo "
		<div>{$name} - Current: {$health} | Min: {$minHealth} Max: {$maxHealth} | {$percentage}%
			<div class='progress {$style}' style='height:22px' role='progressbar' tabindex='0' aria-valuenow='{$health}' aria-valuemin='{$minHealth}' aria-valuemax='{$maxHealth}'>
				  <div class='progress-meter ' style='width: {$percentage}%'></div>
			</div>
		</div>
	";
}


 echo "<h2>Zoo Object</h2><pre>" . print_r($zoo,true) . "</pre>";

?>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.4/foundation.js"></script>
</body>