<?php

namespace Zoo;

class Simulator {

	public 	$simulator;

	private $iterations = 0;
	private $options = [];
	private $data = [];
	private $environment;

	public function __construct($simulation,$options) {

		error_log('Simulator::Construct');

		$this->simulation = $simulation;
		$this->options = $options;

		session_start();

		if(isset($_SESSION[$simulation])) {
			$this->load($_SESSION[$simulation]);
		} else {
			$this->start();
		}

		if(isset($_GET['target'])) {

			$target = $_GET['target'];
			$action = isset($_GET['action']) ? $_GET['action'] : null;

			switch ($target) {
				case 'interact':
					$this->environment->$action();
					//$this->increment();
					break;
				case 'destroy':
					$this->destroy();
					break;
				case 'increment':
					$this->increment();
					break;
			}

		}

		$this->store();

		return $this;

	}

	private function start() {

		error_log('Simulator::start');
		$class = '\\Zoo\\Environment\\'.$this->simulation;
		$this->environment = new $class($this->options);

	}

	private function load() {

		error_log('Simulator::load');
		$this->iterations = $_SESSION[$this->simulation]['iterations'];
		$this->options = $_SESSION[$this->simulation]['options'];
		$class = '\\Zoo\\Environment\\'.$this->simulation;
		$this->environment = new $class();
		$this->environment->load($_SESSION[$this->simulation]['environment'][$this->simulation]);

	}

	public function increment() {
		error_log('Simulator::increment');
		$this->iterations = $this->iterations+1;
		$this->environment->increment();

	}

	private function store() {
		error_log('Simulator::store');
		$_SESSION[$this->simulation]['iterations'] = $this->iterations;
		$_SESSION[$this->simulation]['options'] = $this->options;
		$_SESSION[$this->simulation]['environment'][$this->simulation] = $this->environment->store();

	}

	private function destroy() {
		error_log('Simulator::destroy');
		$_SESSION = array();
		session_destroy();
		$this->start();
		$this->iterations = 0;
	}

	public function output() {
		error_log('Simulator::output');

		$return['iterations'] = $this->iterations;
		$return['animals'] = $this->environment->getAnimals();

		return $return;

	}

}
