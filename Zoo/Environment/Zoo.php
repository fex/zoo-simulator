<?php

namespace Zoo\Environment;

class Zoo {

	private $animals;
	private $resources;

	public function __construct($options=array()) {

		error_log('Zoo::Construct');

		isset($options['animals']) ? $this->createAnimals($options['animals']) : null;

		return $this;

	}

	private function createAnimals($animals) {

		error_log('Zoo::createAnimals');

		foreach($animals as $animal) {
			$class = '\\Zoo\\Animal\\' . $animal;
			$this->animals[] = new $class();
		}

	}

	public function feed() {
		error_log('Zoo::feed');
		foreach($this->animals as $animal) {
			if($animal->status != \Zoo\Animal\Status::Dead) {
				 $currentHealth = $animal->getHealth();
				 $currentHealth = $currentHealth['health'];
				  $randomNumber = (mt_rand() / mt_getrandmax()*15)+10;
				$animal->setHealth($currentHealth*($randomNumber/100));
			}
		}

	}

	public function increment() {
		error_log('Zoo::increment');
		foreach($this->animals as $animal) {
			if($animal->status != \Zoo\Animal\Status::Dead) {
				$animal->setHealth(-(mt_rand() / mt_getrandmax()*20));
			}
		}

	}

	public function load($data) {
		error_log('Zoo::load');
		if(isset($data['animals'])) {
			foreach($data['animals'] as $animal) {
				$this->animals[] = new $animal['class']($animal);
			}
		}

	}

	public function storeAnimals() {

		error_log('Zoo::storeAnimals');
		$return = array();

		if(count($this->animals)) {
			foreach($this->animals as $animal) {
				$return[] = $animal->store();
			}
		}

		return $return;

	}

	public function store() {
		error_log('Zoo::store');

		$return['animals'] = $this->storeAnimals();

		return $return;

	}

	public function getAnimals() {

		$return = $this->animals;

		return $return;

	}

}
