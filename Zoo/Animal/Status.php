<?php

namespace Zoo\Animal;

abstract class Status  {
    const __default = self::Alive;
    const Dead = 0;
    const Lame = 1;
    const Alive = 2;
}