<?php

namespace Zoo\Animal;

class Elephant extends Animal {

	protected $minHealth = 70;

	public function setHealth($health) {
		error_log('Elephant::setHealth');
		$newHealth = $this->health + $health;
		$action = isset($_GET['action']) ? $_GET['action'] : null;
		$this->status = Status::Alive;
		if($newHealth > $this->maxHealth) {
			$this->health = $this->maxHealth;
		} elseif ($this->health > $this->minHealth && $newHealth < $this->minHealth) {
			$this->health = $newHealth;
			$this->status = Status::Lame;
		} elseif ($this->health < $this->minHealth && $newHealth < $this->minHealth && $action != 'feed') {
			$this->health = $newHealth;
			$this->status = Status::Dead;
		} else {
			$this->health = $newHealth;
		}
	}

}