<?php

namespace Zoo\Animal;

abstract class Animal {

	protected $maxHealth = 100;
	protected $minHealth = 30;
	protected $health = 100;

	public $name = Status::Alive;
	public $status = Status::Alive;

	public function __construct($data = null) {
		error_log(get_class($this).'::__construct');
		$this->name = (new \ReflectionClass($this))->getShortName();
		if(is_array($data)) {
			$this->load($data);
		}
		return $this;
	}

	public function setHealth($health) {
		error_log(get_class($this).'::setHealth');
		$newHealth = $this->health + $health;
		$this->status = Status::Alive;
		if($newHealth > $this->maxHealth) {
			$this->health = $this->maxHealth;
		} elseif ($newHealth < $this->minHealth) {
			$this->health = $newHealth;
			$this->status = Status::Dead;
		} else {
			$this->health = $newHealth;
		}

	}

	public function getHealth() {

		error_log(get_class($this).'::getHealth');
		$health['maxHealth'] = $this->maxHealth;
		$health['minHealth'] = $this->minHealth;
		$health['health'] = $this->health;
		$health['status'] = $this->status;
		return $health;

	}

	public function load($data) {
	
		error_log(get_class($this).'::load');
		$return['class'] = get_class($this);
		$this->maxHealth = $data['maxHealth'];
		$this->minHealth = $data['minHealth'];
		$this->health = $data['health'];
		$this->status = $data['status'];

	}

	public function store() {

		error_log(get_class($this).'::store');
		$return['class'] = get_class($this);
		$return['maxHealth'] = $this->maxHealth;
		$return['minHealth'] = $this->minHealth;
		$return['health'] = $this->health;
		$return['status'] = $this->status;

		return $return;

	}
}
